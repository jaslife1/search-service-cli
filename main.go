package main

import (
	"context"
	"log"
	"time"

	"github.com/jaslife1/uniuri"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/search-service/proto/search"
)

func main() {
	srv := micro.NewService(

		micro.Name("search-service-cli"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Create a new client
	c := pb.NewSearchServiceClient("search-service", client.DefaultClient)

	// Search for a store
	{
		id := uniuri.New()

		start := time.Now()
		log.Println("Search")
		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		query := "levis"
		resp, err := c.Search(ctx, &pb.Query{Query: query})

		if err != nil {
			log.Panicf("Error: Did not get to search for %s - %v", query, err)
		}

		log.Printf("Nodes: %v", resp.Nodes)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}
}
