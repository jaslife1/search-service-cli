build:
	docker build -t search_service_cli .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		search_service_cli